#include <opencv2/opencv.hpp>
#include <vector>
//Kernel permettande de passer une image RGB en noir et blanc
__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t cols, std::size_t rows ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i < cols && j < rows ) {
    g[ j * cols + i ] = (
			 307 * rgb[ 3 * ( j * cols + i ) ]
			 + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
			 + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
			 ) / 1024;
  }
}
//Kernel appliquant le filtre laplacian operator 
__global__ void laplacian_operator(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * gaussian){

  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;

  if(i > 0 && i < cols && j < rows && j > 0){

	auto h = 4 * g[((j    ) * cols + i    ) ] -  g[((j    ) * cols + i + 1) ]
           -   g[((j + 1) * cols + i    ) ] -  g[((j - 1) * cols + i    ) ]
           -   g[((j    ) * cols + i - 1) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	gaussian[ j * cols + i ] = sqrtf(res);

 }
}
//Kernel appliquant le filtre laplacian operator avec mémoire partagée
__global__ void laplacian_operator_shared(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * laplacian){

  auto i = blockIdx.x * (blockDim.x-2) + threadIdx.x;
  auto j = blockIdx.y * (blockDim.y-2) + threadIdx.y;

  auto li = threadIdx.x;
  auto lj = threadIdx.y;

  auto w = blockDim.x;
  auto h = blockDim.y;

  extern __shared__ unsigned char sh[];

  if( i < cols && j < rows )
  {
    sh[ lj * w + li ] = g[ j * cols + i ];
  }

  __syncthreads();


  if( i < cols -1 && j < rows-1 && li > 0 && li < (w-1) && lj > 0 && lj < (h-1) ) {

	auto h = 4 * sh[((lj    ) * w + li    ) ] -  sh[((lj    ) * w + li + 1) ]
           -   sh[((lj + 1) * w + li    ) ] -  sh[((lj - 1) * w + li    ) ]
           -   sh[((lj    ) * w + li - 1) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	laplacian[ j * cols + i ] = sqrtf(res);

 }
}


//Fonction permettant d'avoir un retour si une erreur se produit
void checkError(cudaError_t error, const std::string& errorMessage) {
  if (error != cudaSuccess) {
    std::cout << errorMessage << std::endl;
  }
}

int main(int argc, char *argv[])
{
  //Choix de l'image

  //cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
  //cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );

  cudaError_t cudaStatus;
  cudaError_t kernelStatus;

  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;

  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  cv::Mat m_grayscale( rows, cols, CV_8UC1, g.data() );

  unsigned char * rgb_d;
  unsigned char * g_d;
  unsigned char * result_d;

  cudaStatus = cudaMalloc( &rgb_d, 3 * rows * cols );
  checkError(cudaStatus,(std::string) "Error CudaMalloc rgb_d");

  cudaStatus = cudaMalloc( &g_d, rows * cols );
  checkError(cudaStatus,(std::string) "Error CudaMalloc g_d");

  cudaStatus = cudaMalloc( &result_d, rows*cols);
  checkError(cudaStatus,(std::string) "Error CudaMalloc result_d");

  
  cudaStatus = cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );
  checkError(cudaStatus,(std::string) "Error cudaMemcpy Host > Device rgb_d");

  //taille blocks et threads sans mémoire partagée
  dim3 t( 32, 32 );
  dim3 b((cols-1)/t.x+1,(rows-1)/t.y+1);

  //taille blocks et threads avec mémoire partagée
  dim3 t_shared( 32, 4 );
  dim3 b_shared((cols-1)/(t_shared.x-2)+1, (rows-1)/(t_shared.y-2)+1);

  cudaEvent_t start, stop;

  cudaEventCreate( &start );
  cudaEventCreate( &stop );

  //Application du filtre Grayscale
  grayscale<<< b, t >>>( rgb_d, g_d, cols, rows );

  // Mesure du temps de calcul du kernel uniquement.
  cudaEventRecord( start );

  //version sans mémoire partagée
  //laplacian_operator<<< b, t >>>(g_d, cols, rows, result_d);

  //version avec mémoire partagée
  laplacian_operator_shared<<< b_shared, t_shared, t_shared.x * t_shared.y >>>(g_d, cols, rows, result_d);

  cudaEventRecord( stop );
  cudaEventSynchronize( stop );

  float duration;
  cudaEventElapsedTime( &duration, start, stop );
  std::cout << "time = " << duration << "ms" << std::endl;

  kernelStatus = cudaGetLastError();
  checkError(cudaStatus,(std::string) "CUDA Error : "+cudaGetErrorString(kernelStatus));

  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  //copie de result_d vers g.data() afin d'avoir l'image de sortie
  cudaStatus = cudaMemcpy( g.data(), result_d, rows * cols, cudaMemcpyDeviceToHost );
  checkError(cudaStatus,(std::string) "Error cudaMemcpy Device > Host result_d");

  cv::imwrite( "Images/Output/out_laplacian_operator_cu.jpg", m_out );
  cudaFree( rgb_d);
  cudaFree( g_d);
  cudaFree( result_d);

  
  return 0;
}