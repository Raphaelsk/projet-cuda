#include <opencv2/opencv.hpp>
#include <vector>
#include <cstring>

/**
 * Kernel pour transformer l'image RGB en niveaux de gris.
 */
__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t cols, std::size_t rows ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i < cols && j < rows ) {
    g[ j * cols + i ] = (
			 307 * rgb[ 3 * ( j * cols + i ) ]
			 + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
			 + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
			 ) >> 10;
  }
}

/**
 * Application du filtre laplacian operator
*/
__global__ void laplacian_operator(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * gaussian){

  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;

  if(i > 0 && i < cols && j < rows && j > 0){

	auto h = 4 * g[((j    ) * cols + i    ) ] -  g[((j    ) * cols + i + 1) ]
           -   g[((j + 1) * cols + i    ) ] -  g[((j - 1) * cols + i    ) ]
           -   g[((j    ) * cols + i - 1) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	gaussian[ j * cols + i ] = sqrtf(res);

 }
}

/**
 * Application du filtre laplacian operator avec mémoire partagée
*/
__global__ void laplacian_operator_shared(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * laplacian){
  // x et y global avec ghost
  auto i = blockIdx.x * (blockDim.x-2) + threadIdx.x;
  auto j = blockIdx.y * (blockDim.y-2) + threadIdx.y;
  // x et y local au block
  auto li = threadIdx.x;
  auto lj = threadIdx.y;

  auto w = blockDim.x;
  auto h = blockDim.y;
  // Déclaration du tableau de mémoire partagée
  extern __shared__ unsigned char sh[];

  if( i < cols && j < rows )
  {
    sh[ lj * w + li ] = g[ j * cols + i ];
  }
  // Synchronisation des threads après remplissage du tableau
  __syncthreads();

  // Accès plus rapide à la mémoire partagée que de faire appel au tableau g à chaque fois
  if( i < cols -1 && j < rows-1 && li > 0 && li < (w-1) && lj > 0 && lj < (h-1) ) {

	auto h = 4 * sh[((lj    ) * w + li    ) ] -  sh[((lj    ) * w + li + 1) ]
           -   sh[((lj + 1) * w + li    ) ] -  sh[((lj - 1) * w + li    ) ]
           -   sh[((lj    ) * w + li - 1) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	laplacian[ j * cols + i ] = sqrtf(res);

 }
}

// Fonction permettant de récupérer les erreurs renvoyées par les fonctions cuda
void checkError(cudaError_t error, const std::string& errorMessage) {
  if (error != cudaSuccess) {
    std::cout << errorMessage << std::endl;
  }
}

int main()
{
// Les trois images utilisés pour les filtres
//cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
//cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );

  cudaError_t cudaStatus;
  cudaError_t kernelStatus;

  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;

  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  cv::Mat m_grayscale( rows, cols, CV_8UC1, g.data() );

  unsigned char * rgb_d;
  unsigned char * g_d;
  // Allocation mémoire dans le GPU
  cudaMalloc( &rgb_d, 3 * rows * cols );
  cudaMalloc( &g_d, rows * cols );
  cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );
  // Déclaration des tailles de block et nombre de block
  dim3 t( 32, 32 );
  dim3 b((cols-1)/t.x+1,(rows-1)/t.y+1);

  dim3 t_shared( 32, 6 );
  dim3 b_shared((cols-1)/(t_shared.x-4)+1, (rows-1)/(t_shared.y-4)+1);

  std::size_t const size = cols*rows;
  std::size_t const sizeb = size * sizeof( char );
  
  unsigned char * v0 = nullptr;
  unsigned char * v1 = nullptr;

  unsigned char * v0_d = nullptr;
  unsigned char * v1_d = nullptr;

  cudaMalloc( &v0_d, sizeb );
  cudaMalloc( &v1_d, sizeb );
	
  cudaMallocHost( &v0, sizeb );
  cudaMallocHost( &v1, sizeb );
  // Création des streams
  cudaStream_t streams[ 2 ];

  cudaStreamCreate( &streams[ 0 ] );
  cudaStreamCreate( &streams[ 1 ] );
    
  cudaEvent_t start, stop;

  cudaEventCreate( &start );
  cudaEventCreate( &stop );

  // Version en 2 étapes.
  grayscale<<<  b, t >>>( rgb_d, g_d, cols, rows );
  cudaMemcpy( g.data(), g_d, rows * cols, cudaMemcpyDeviceToHost );
  // Copie des données de g vers la mémoire fixé sur la RAM
  for( std::size_t i = 0 ; i < size ; ++i )
  {
      v0[ i ] = g.data()[i];
  }
  
  cudaStatus = cudaMemcpyAsync( v0_d, v0, ((size/2)+1*cols) * sizeof(char), cudaMemcpyHostToDevice, streams[ 0 ] );
  checkError(cudaStatus,(std::string) "Error cudaMemcpyAsync Device < Host v0 streams 0");

  cudaStatus = cudaMemcpyAsync( (v0_d+(size/2))-1*cols, (v0+(size/2))-1*cols, ((size/2)+1*cols) * sizeof(char), cudaMemcpyHostToDevice, streams[ 1 ] );
  checkError(cudaStatus,(std::string) "Error cudaMemcpyAsync Device < Host v1_d streams 1");

  cudaEventRecord( start );
  // Utilisation du filtre laplacian operator avec streams sans shared memory
  //laplacian_operator<<< b, t, 0, streams[ 0 ] >>>( v0_d, cols, ((rows/2)+1),v1_d);
  //laplacian_operator<<< b, t, 0, streams[ 1 ] >>>( (v0_d+size/2)-1*cols, cols, ((rows/2)+1),v1_d+(size/2)-1*cols);

  // Utilisation du filtre laplacian operator avec streams et shared memory
  laplacian_operator_shared<<< b_shared, t_shared, t_shared.x * t_shared.y , streams[0] >>>( v0_d, cols, ((rows/2)+1),v1_d);
  laplacian_operator_shared<<< b_shared, t_shared, t_shared.x * t_shared.y , streams[1] >>>((v0_d+size/2)-1*cols, cols, ((rows/2)+1),v1_d+(size/2)-1*cols);

  // Synchronisation des 2 streams pour avoir un temps cohérent
  cudaStreamSynchronize(streams[0]);
  cudaStreamSynchronize(streams[1]);
  
  cudaEventRecord( stop );
  // Transfert des données du GPU vers le CPU après traitement des données
  cudaStatus = cudaMemcpyAsync( v1, v1_d, (size/2) * sizeof(char), cudaMemcpyDeviceToHost, streams[ 0 ] );
  checkError(cudaStatus,(std::string) "Error cudaMemcpyAsync Device > Host v1_d streams 0");

  cudaStatus = cudaMemcpyAsync( v1+(size/2), v1_d+(size/2), (size/2) * sizeof(char), cudaMemcpyDeviceToHost, streams[ 1 ] );
  checkError(cudaStatus,(std::string) "Error cudaMemcpyAsync Device > Host v1_d streams 1");
  // Synchronisation pour rendre le programme synchrone
  cudaDeviceSynchronize();

  // Récupération du filtre appliqué aux données de l'image pour avoir l'image en sortie
  cudaStatus = cudaMemcpy( g.data(), v1, rows * cols, cudaMemcpyDeviceToHost );
  checkError(cudaStatus,(std::string) "Error cudaMemcpy Device > Host v1 > g.data()");

  cudaEventSynchronize( stop );

  float duration;

  // Récupération des erreurs
  kernelStatus = cudaGetLastError();
  checkError(cudaStatus,(std::string) "CUDA Error : "+cudaGetErrorString(kernelStatus));

  // Affichage du temps enregistrer
  cudaEventElapsedTime( &duration, start, stop );
  std::cout << "time=" << duration << std::endl;

  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  cv::imwrite( "Images/Output/out_laplacian_operator_streams_cu.jpg", m_out );
  cudaFree( rgb_d);
  cudaFree( g_d);
  cudaStreamDestroy( streams[ 0 ] );
  cudaStreamDestroy( streams[ 1 ] );
  cudaFree( v0_d );
  cudaFree( v1_d );
  cudaFreeHost( v0 );
  cudaFreeHost( v1 );
  
  return 0;
}
