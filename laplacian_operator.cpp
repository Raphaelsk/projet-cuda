#include <opencv2/opencv.hpp>

#include <vector>
#include <iostream>
#include <cmath>
#include <chrono>


int main() {
  //Choix de l'image

  cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  //cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
  //cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );

  auto rgb = m_in.data;

  auto width = m_in.cols;
  auto height = m_in.rows;

  std::vector< unsigned char > g( m_in.rows * m_in.cols );
  cv::Mat m_out( m_in.rows, m_in.cols, CV_8UC1, g.data() );

  unsigned char* out_grey = new unsigned char[ width*height ];
  //Application de grayscale
  for( std::size_t i = 0 ; i < width*height ; ++i )
  {
    // GREY = ( 307 * R + 604 * G + 113 * B ) / 1024
    out_grey[ i ] = ( 307 * rgb[ 3*i ]
		       + 604 * rgb[ 3*i+1 ]
		       + 113 * rgb[ 3*i+2 ]
		       ) >> 10;
  }

  auto start = std::chrono::system_clock::now();
  
  unsigned int i, j;

  int h, res;
  //Application de laplacian operator
  for(j = 2 ; j < height - 2 ; ++j) {

    for(i = 2 ; i < width - 2 ; ++i) {

      h = 4 * out_grey[((j    ) * width + i    ) ] -  out_grey[((j    ) * width + i + 1) ]
           -   out_grey[((j + 1) * width + i    ) ] -  out_grey[((j - 1) * width + i    ) ]
           -   out_grey[((j    ) * width + i - 1) ];

        res = h*h;
        res = res > 255*255 ? res = 255*255 : res;

        g[ j * width + i ] = sqrt(res);

    }

  }

  auto stop = std::chrono::system_clock::now();

  auto duration = stop - start;
  auto ms = std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count();

  std::cout << ms << " ms" << std::endl;

  cv::imwrite( "Images/Output/out_laplacian_operator_cpp.jpg", m_out );

  delete [] out_grey;
}