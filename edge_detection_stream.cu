#include <opencv2/opencv.hpp>
#include <vector>

__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t cols, std::size_t rows ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i < cols && j < rows ) {
    g[ j * cols + i ] = 
        (
          307 * rgb[ 3 * ( j * cols + i ) ]
          + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
          + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
        ) / 1024;
  }
}

__global__ void edge_detection_shared(unsigned char * g, unsigned char * edge, std::size_t cols, std::size_t rows){
  // x et y local au block
  auto lx = threadIdx.x;
  auto ly = threadIdx.y;

  auto w = blockDim.x;
  auto h = blockDim.y;

  // x et y global avec ghost
  auto x = blockIdx.x * (blockDim.x-2) + threadIdx.x;
  auto y = blockIdx.y * (blockDim.y-2) + threadIdx.y;

  // Déclaration du tableau de mémoire partagée
  extern __shared__ unsigned char sh[];

  if( x < cols && y < rows )
  {
    sh[ ly * w + lx ] = g[ y * cols + x ];
  }

  // Synchronisation des threads après remplissage du tableau
  __syncthreads();

  // Accès plus rapide à la mémoire partagée que de faire appel au tableau g à chaque fois
  if(x < cols - 1 && y < rows - 1 && lx > 0 && lx < (w-1) && ly > 0 && ly < (h-1)){
    auto h = 
        -sh[((ly - 1) * w + lx - 1) ] -sh[(ly-1) * w + lx] -sh[((ly - 1) * w + lx + 1)]
        -sh[( ly      * w + lx - 1) ] + 8 * sh[ ly * w + lx] -sh[( ly      * w + lx + 1) ]
        -sh[((ly + 1) * w + lx - 1) ] -sh[(ly+1) * w + lx] -sh[((ly + 1) * w + lx + 1)];

    auto res = h*h;
    res = res > 65025 ? 65025 : res;
    edge[ y * cols + x ] = sqrtf(res);
  }
}

__global__ void edge_detection(unsigned char * g, unsigned char * edge, std::size_t cols, std::size_t rows){
  // x et y global
  auto x = blockIdx.x * blockDim.x + threadIdx.x;
  auto y = blockIdx.y * blockDim.y + threadIdx.y;

  if(x > 0 && x < cols && y < rows && y > 0){
    auto h = 
        -g[((y - 1) * cols + x - 1) ] -g[(y-1) * cols + x] -g[((y - 1) * cols + x + 1)]
        -g[( y      * cols + x - 1) ] + 8 * g[ y * cols + x] -g[( y      * cols + x + 1) ]
        -g[((y + 1) * cols + x - 1) ] -g[(y+1) * cols + x] -g[((y + 1) * cols + x + 1)];

    auto res = h*h;
    res = res > 65025 ? 65025 : res;
    edge[ y * cols + x ] = sqrtf(res);
  }
}

// Fonction permettant de récupérer les erreurs renvoyées par les fonctions cuda
void checkError(cudaError_t error)
{
  if (error != cudaSuccess)
  {
    std::cout << cudaGetErrorName(error) << " : " << cudaGetErrorString(error) << std::endl;
    exit((int) error);
  }
}

int main()
{
  // Les trois images utilisés pour les filtres
  //cv::Mat m_in = cv::imread("./Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  //cv::Mat m_in = cv::imread("./Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
  cv::Mat m_in = cv::imread("./Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );  

  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;

  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data());

  unsigned char * rgb_d;
  unsigned char * g_d;
  unsigned char * edge_d;

  std::size_t const size = rows * cols;
  std::size_t const sizeBits = size * sizeof(unsigned char); 

  // Allocation mémoire dans le GPU
  checkError(cudaMalloc(&rgb_d, 3 * size));
  checkError(cudaMalloc(&g_d, size));
  checkError(cudaMalloc(&edge_d, size));

  checkError(cudaMemcpy(rgb_d, rgb, 3 * size, cudaMemcpyHostToDevice));

  // Déclaration des tailles de block et nombre de block
  dim3 t( 32, 32 );
  dim3 b( ( cols - 1) / t.x + 1 , ( rows - 1 ) / t.y + 1 );
  dim3 grid1( ( cols - 1) / (t.x-2) + 1 , ( rows - 1 ) / (t.y-2) + 1 );

  // Création des streams
  cudaStream_t streams[2];

  cudaStreamCreate(&streams[0]);
  cudaStreamCreate(&streams[1]);

  cudaEvent_t start, stop;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  // En copiant les données dans g et en les plaçant dans la mémoire fixé sur la RAM les streams sont un peu plus rapide
  grayscale<<<b, t>>>(rgb_d, g_d, cols, rows);
  cudaMemcpy(g.data(), g_d, size, cudaMemcpyDeviceToHost);

  unsigned char * gr = nullptr;
  checkError(cudaMallocHost(&gr, sizeBits));

  // Copie des données de g vers la mémoire fixé sur la RAM
  for(std::size_t i = 0 ; i < size; ++i)
  {
    gr[i] = g[i];
  }

  // Recopie des données vers g_d
  checkError(cudaMemcpyAsync(g_d, gr, (size / 2 + cols) * sizeof(unsigned char), cudaMemcpyHostToDevice, streams[0]));
  checkError(cudaMemcpyAsync(g_d + (size / 2 - cols), gr + (size / 2 - cols), (size / 2 + cols) * sizeof(unsigned char), cudaMemcpyHostToDevice, streams[1]));

  cudaEventRecord(start);

  // Utilisation du filtre edge_detection avec streams sans shared memory
  //edge_detection<<< b, t, 0, streams[0] >>>(g_d, edge_d, cols, rows / 2 + 1);
  //edge_detection<<< b, t, 0, streams[1] >>>(g_d + (size / 2 - cols), edge_d + (size / 2), cols, rows / 2 + 1);

  // Utilisation du filtre edge_detection avec streams et shared memory
  edge_detection_shared<<< grid1, t, t.x * t.y, streams[0] >>>(g_d, edge_d, cols, rows / 2 + 1);
  edge_detection_shared<<< grid1, t, t.x * t.y, streams[1] >>>(g_d + (size / 2 - cols), edge_d + (size / 2), cols, rows / 2 + 1);

  //edge_detection_shared<<< grid1, t, t.x * t.y >>>(g_d, edge_d, cols, rows);

  // Synchronisation des 2 streams pour avoir un temps cohérent
  cudaStreamSynchronize(streams[0]);
  cudaStreamSynchronize(streams[1]);

  cudaEventRecord(stop);

  // Transfert des données du GPU vers le CPU après traitement des données
  checkError(cudaMemcpyAsync(gr, edge_d, size / 2 * sizeof(unsigned char), cudaMemcpyDeviceToHost, streams[0]));
  checkError(cudaMemcpyAsync(gr + (size / 2), edge_d + (size / 2), size / 2 * sizeof(unsigned char), cudaMemcpyDeviceToHost, streams[1]));

  // Synchronisation pour rendre le programme synchrone
  cudaDeviceSynchronize();

  // Récupération du filtre appliqué aux données de l'image pour avoir l'image en sortie
  cudaMemcpy(g.data(), gr, size, cudaMemcpyHostToHost);

  // Affichage du temps enregistrer
  cudaEventSynchronize(stop);
  float duration;
  cudaEventElapsedTime( &duration, start, stop );
  std::cout << "time = " << duration << "ms" <<std::endl;

  // Libération de la mémoire alloué sur le GPU
  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  cudaStreamDestroy(streams[0]);
  cudaStreamDestroy(streams[1]);

  cv::imwrite( "./Images/Output/out_edge_detection_streams_cu.jpg", m_out );
  cudaFree( rgb_d);
  cudaFree( g_d);
  cudaFree( edge_d);

  // Libération de la mémoire alloué sur le CPU par le GPU
  cudaFreeHost(gr);
  return 0;
}
