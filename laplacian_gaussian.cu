#include <opencv2/opencv.hpp>
#include <vector>

__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t cols, std::size_t rows ) {
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i < cols && j < rows ) {
    g[ j * cols + i ] = (
			 307 * rgb[ 3 * ( j * cols + i ) ]
			 + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
			 + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
			 ) / 1024;
  }
}

__global__ void laplacian_gaussian(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * gaussian){
  // x et y global
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;

  if(i > 1 && i < cols && j < rows && j > 1){

	auto h = 16 * g[((j    ) * cols + i    ) ] -  g[((j    ) * cols + i + 2) ] 
          -     g[((j - 1) * cols + i + 1) ]
          - 2 * g[((j    ) * cols + i + 1) ] -     g[((j + 1) * cols + i + 1) ]
            - 2 * g[((j - 1) * cols + i    ) ]
          - 2 * g[((j + 1) * cols + i    ) ] 
          -     g[((j - 2) * cols + i    ) ] -     g[((j + 2) * cols + i    ) ]
          -     g[((j - 1) * cols + i - 1) ] -     g[((j + 1) * cols + i - 1) ]
          - 2 * g[((j    ) * cols + i - 1) ] -     g[((j    ) * cols + i - 2) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	gaussian[ j * cols + i ] = sqrtf(res);
 }
}

// Fonction permettant de récupérer les erreurs renvoyées par les fonctions cuda
void checkError(cudaError_t error, const std::string& errorMessage) {
  if (error != cudaSuccess) {
    std::cout << errorMessage << std::endl;
  }
}

__global__ void laplacian_gaussian_shared(unsigned char * g, std::size_t cols, std::size_t rows, unsigned char * gaussian){
  // x et y global avec ghost
  auto i = blockIdx.x * (blockDim.x-4) + threadIdx.x;
  auto j = blockIdx.y * (blockDim.y-4) + threadIdx.y;

  // x et y local au block
  auto li = threadIdx.x;
  auto lj = threadIdx.y;

  auto w = blockDim.x;
  auto h = blockDim.y;

  // Déclaration du tableau de mémoire partagée
  extern __shared__ unsigned char sh[];

  if( i < cols && j < rows )
  {
    sh[ lj * w + li ] = g[ j * cols + i ];
  }

  // Synchronisation des threads après remplissage du tableau
  __syncthreads();

  // Accès plus rapide à la mémoire partagée que de faire appel au tableau g à chaque fois
  if( i < cols -2 && j < rows-2 && li > 1 && li < (w-2) && lj > 1 && lj < (h-2) ) {

    auto h = 16 *sh[((lj    ) * w + li    ) ] -  sh[((lj    ) * w + li + 2) ] 
          -     sh[((lj - 1) * w + li + 1) ]
          - 2 * sh[((lj    ) * w + li + 1) ] -     sh[((lj + 1) * w + li + 1) ]
            - 2 * sh[((lj - 1) * w + li    ) ]
          - 2 * sh[((lj + 1) * w + li    ) ] 
          -     sh[((lj - 2) * w + li    ) ] -     sh[((lj + 2) * w + li    ) ]
          -     sh[((lj - 1) * w + li - 1) ] -     sh[((lj + 1) * w + li - 1) ]
          - 2 * sh[((lj    ) * w + li - 1) ] -     sh[((lj    ) * w + li - 2) ];

	auto res = h*h;
	res = res > 65535 ? res = 65535 : res;
	gaussian[ j * cols + i ] = sqrtf(res);

 }
}

int main(int argc, char *argv[])
{
  // Les trois images utilisés pour les filtres
  //cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
  //cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );

  cudaError_t cudaStatus;
  cudaError_t kernelStatus;

  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;

  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  cv::Mat m_grayscale( rows, cols, CV_8UC1, g.data() );

  unsigned char * rgb_d;
  unsigned char * g_d;
  unsigned char * result_d;

  // Allocation mémoire dans le GPU
  cudaStatus = cudaMalloc( &rgb_d, 3 * rows * cols );
  checkError(cudaStatus,(std::string) "Error CudaMalloc rgb_d");

  cudaStatus = cudaMalloc( &g_d, rows * cols );
  checkError(cudaStatus,(std::string) "Error CudaMalloc g_d");

  cudaStatus = cudaMalloc( &result_d, rows*cols);
  checkError(cudaStatus,(std::string) "Error CudaMalloc result_d");

  cudaStatus = cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );
  checkError(cudaStatus,(std::string) "Error cudaMemcpy Host > Device rgb_d");

  // Déclaration des tailles de block et nombre de block
  dim3 t( 32, 32 );
  dim3 b((cols-1)/t.x+1,(rows-1)/t.y+1);

  dim3 t_shared( 32, 6 );
  dim3 b_shared((cols-1)/(t_shared.x-4)+1, (rows-1)/(t_shared.y-4)+1);

  cudaEvent_t start, stop;

  cudaEventCreate( &start );
  cudaEventCreate( &stop );

  grayscale<<< b, t >>>( rgb_d, g_d, cols, rows );

  // Mesure du temps de calcul du kernel uniquement.
  cudaEventRecord( start );

  // Filtre laplacian gaussian sans shared memory
  //laplacian_gaussian<<< b, t >>>(g_d, cols, rows, result_d);

  // Filtre laplacian gaussian avec shared memory
  laplacian_gaussian_shared<<< b_shared, t_shared, t_shared.x * t_shared.y >>>(g_d, cols, rows, result_d);

  cudaEventRecord( stop );

  // Récupération du filtre appliqué aux données de l'image pour avoir l'image en sortie
  cudaStatus = cudaMemcpy( g.data(), result_d, rows * cols, cudaMemcpyDeviceToHost );
  checkError(cudaStatus,(std::string) "Error cudaMemcpy Device > Host result_d");

  // Affichage du temps enregistrer
  cudaEventSynchronize( stop );
  float duration;
  cudaEventElapsedTime( &duration, start, stop );
  std::cout << "time = " << duration << "ms" <<std::endl;

  kernelStatus = cudaGetLastError();
  checkError(cudaStatus,(std::string) "CUDA Error : "+cudaGetErrorString(kernelStatus));
  
  // Libération de la mémoire alloué sur le GPU
  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  cv::imwrite( "Images/Output/out_laplacian_gaussian_cu.jpg", m_out );
  cudaFree( rgb_d);
  cudaFree( g_d);
  cudaFree( result_d);

  return 0;
}