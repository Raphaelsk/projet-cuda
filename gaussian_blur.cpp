#include <opencv2/opencv.hpp>

#include <vector>
#include <iostream>
#include <cmath>
#include <chrono>

int main()
{

  // cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  // cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED );
  cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED);

  auto rgb = m_in.data;
  auto width = m_in.cols;
  auto height = m_in.rows;

  std::vector<unsigned char> result(m_in.rows * m_in.cols);
  cv::Mat m_out(m_in.rows, m_in.cols, CV_8UC1, result.data());
  //application de grayscale
  unsigned char *out_grey = new unsigned char[width * height];

  for (std::size_t i = 0; i < width * height; ++i)
  {
    // GREY = ( 307 * R + 604 * G + 113 * B ) / 1024
    out_grey[i] = (307 * rgb[3 * i] + 604 * rgb[3 * i + 1] + 113 * rgb[3 * i + 2]) >> 10;
  }

  unsigned int i, j;

  int h, res;
  //application du filtre gaussian blur
  auto start = std::chrono::system_clock::now();
  for (std::size_t j = 3; j < height - 3; ++j)
  {
    for (std::size_t i = 3; i < width - 3; ++i)
    {
      auto h = ((
                    5 * out_grey[((j - 3) * width + i)]

                    + 5 * out_grey[((j - 2) * width + i - 2)] + 18 * out_grey[((j - 2) * width + i - 1)] + 32 * out_grey[((j - 2) * width + i)] + 18 * out_grey[((j - 2) * width + i + 1)] + 5 * out_grey[((j - 2) * width + i + 2)]

                    + 18 * out_grey[((j - 1) * width + i - 2)] + 64 * out_grey[((j - 1) * width + i - 1)] + 100 * out_grey[((j - 1) * width + i)] + 64 * out_grey[((j - 1) * width + i + 1)] + 18 * out_grey[((j - 1) * width + i + 2)]

                    + 5 * out_grey[(j * width + i - 3)] + 32 * out_grey[(j * width + i - 2)] + 100 * out_grey[(j * width + i - 1)] + 100 * out_grey[(j * width + i)] + 100 * out_grey[(j * width + i + 1)] + 32 * out_grey[(j * width + i + 2)] + 5 * out_grey[(j * width + i + 3)]

                    + 18 * out_grey[((j + 1) * width + i - 2)] + 64 * out_grey[((j + 1) * width + i - 1)] + 100 * out_grey[((j + 1) * width + i)] + 64 * out_grey[((j + 1) * width + i + 1)] + 18 * out_grey[((j + 1) * width + i + 2)]

                    + 5 * out_grey[((j + 2) * width + i - 2)] + 18 * out_grey[((j + 2) * width + i - 1)] + 32 * out_grey[((j + 2) * width + i)] + 18 * out_grey[((j + 2) * width + i + 1)] + 5 * out_grey[((j + 2) * width + i + 2)]

                    + 5 * out_grey[((j + 3) * width + i)]) /
                1068);
      auto res = h * h;
      res = res > 65025 ? res = 65025 : res;

      result[j * width + i] = sqrtf(res);
    }
  }

  auto stop = std::chrono::system_clock::now();

  auto duration = stop - start;
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

  std::cout << ms << " ms" << std::endl;

  cv::imwrite("Images/Output/out_gaussian_blur_cpp.jpg", m_out);

  delete[] out_grey;
}
