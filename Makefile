CXX=g++
CXXFLAGS=-O3 -march=native
LDLIBS1=`pkg-config --libs opencv`
LDLIBS2=-lm -lIL

all: laplacian_gaussian-cu laplacian_operator-cu laplacian_operator laplacian_gaussian laplacian_gaussian-streams laplacian_operator-streams gaussian_blur gaussian_blur-cu gaussian_blur_streams-cu edge_detection edge_detection_cu edge_detection_cu_stream

laplacian_gaussian-cu: laplacian_gaussian.cu
	nvcc -o $@ $<  $(LDLIBS1)

laplacian_operator-cu: laplacian_operator.cu
	nvcc -o $@ $<  $(LDLIBS1)

laplacian_operator: laplacian_operator.cpp
		$(CXX) $(CXXFLAGS) -o $@ $< $(LDLIBS1)

laplacian_gaussian: laplacian_gaussian.cpp
		$(CXX) $(CXXFLAGS) -o $@ $< $(LDLIBS1)

laplacian_gaussian-streams: laplacian_gaussian_streams.cu
		nvcc --default-stream per-thread -o $@ $<  $(LDLIBS1)

laplacian_operator-streams: laplacian_operator_streams.cu
		nvcc --default-stream per-thread -o $@ $<  $(LDLIBS1)		

gaussian_blur: gaussian_blur.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDLIBS1)

gaussian_blur-cu: gaussian_blur.cu
	nvcc --default-stream per-thread -o $@ $<  $(LDLIBS1)

gaussian_blur_streams-cu: 	gaussian_blur_streams.cu
	nvcc --default-stream per-thread -o $@ $<  $(LDLIBS1)

edge_detection: edge_detection.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDLIBS1)

edge_detection_cu: edge_detection.cu
	nvcc -o $@ $<  $(LDLIBS1)

edge_detection_cu_stream: edge_detection_stream.cu
	nvcc --default-stream per-thread -o $@ $<  $(LDLIBS1)

.PHONY: clean

clean:
	rm laplacian_operator-cu laplacian_operator laplacian_gaussian laplacian_gaussian-cu Images/Output/out_* gaussian_blur gaussian_blur-cu gaussian_blur_streams-cu edge_detection edge_detection_cu edge_detection_cu_stream
