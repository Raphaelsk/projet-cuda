#include <opencv2/opencv.hpp>
#include <vector>
#include <cstring>

// kernel pour passer une image rgb en noir et blanc
__global__ void grayscale(unsigned char *rgb, unsigned char *g, std::size_t cols, std::size_t rows)
{
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if (i < cols && j < rows)
  {
    g[j * cols + i] = (307 * rgb[3 * (j * cols + i)] + 604 * rgb[3 * (j * cols + i) + 1] + 113 * rgb[3 * (j * cols + i) + 2]) >> 10;
  }
}
// kernel pour appliquer le flou gaussien
__global__ void gaussianBlur(unsigned char *g, unsigned char *result, std::size_t cols, std::size_t rows)
{
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;

  if (i >= 3 && i < cols && j >= 3 && j < rows)
  {
    auto h = ((
                  5 * g[((j - 3) * cols + i)]

                  + 5 * g[((j - 2) * cols + i - 2)] + 18 * g[((j - 2) * cols + i - 1)] + 32 * g[((j - 2) * cols + i)] + 18 * g[((j - 2) * cols + i + 1)] + 5 * g[((j - 2) * cols + i + 2)]

                  + 18 * g[((j - 1) * cols + i - 2)] + 64 * g[((j - 1) * cols + i - 1)] + 100 * g[((j - 1) * cols + i)] + 64 * g[((j - 1) * cols + i + 1)] + 18 * g[((j - 1) * cols + i + 2)]

                  + 5 * g[(j * cols + i - 3)] + 32 * g[(j * cols + i - 2)] + 100 * g[(j * cols + i - 1)] + 100 * g[(j * cols + i)] + 100 * g[(j * cols + i + 1)] + 32 * g[(j * cols + i + 2)] + 5 * g[(j * cols + i + 3)]

                  + 18 * g[((j + 1) * cols + i - 2)] + 64 * g[((j + 1) * cols + i - 1)] + 100 * g[((j + 1) * cols + i)] + 64 * g[((j + 1) * cols + i + 1)] + 18 * g[((j + 1) * cols + i + 2)]

                  + 5 * g[((j + 2) * cols + i - 2)] + 18 * g[((j + 2) * cols + i - 1)] + 32 * g[((j + 2) * cols + i)] + 18 * g[((j + 2) * cols + i + 1)] + 5 * g[((j + 2) * cols + i + 2)]

                  + 5 * g[((j + 3) * cols + i)]) /
              1068);
    auto res = h * h;
    res = res > 65025 ? res = 65025 : res;

    result[j * cols + i] = sqrtf(res);
  }
}
// fonction pour avoir les erreur sous forme de texte
void checkError(cudaError_t error, const std::string& errorMessage) {
  if (error != cudaSuccess) {
    std::cout << errorMessage << std::endl;
  }
}
// Kernel pour appliquer le flou gaussien avec utilisation de la mémoire partagée
__global__ void gaussianBlur_shared(unsigned char *g, unsigned char *result, std::size_t cols, std::size_t rows)
{
  auto li = threadIdx.x;
  auto lj = threadIdx.y;

  auto w = blockDim.x;
  auto h = blockDim.y;

  auto i = blockIdx.x * (blockDim.x - 6) + threadIdx.x;
  auto j = blockIdx.y * (blockDim.y - 6) + threadIdx.y;

  extern __shared__ unsigned char sh[];

  if (i < cols && j < rows)
  {
    sh[lj * w + li] = g[j * cols + i];
  }

  __syncthreads();

  if (i < cols - 3 && j < rows - 3 && li > 2 && li < (w - 3) && lj > 2 && lj < (h - 3))
  {
    auto h = ((
                  5 * sh[((lj - 3) * w + li)]

                  + 5 * sh[((lj - 2) * w + li - 2)] + 18 * sh[((lj - 2) * w + li - 1)] + 32 * sh[((lj - 2) * w + li)] + 18 * sh[((lj - 2) * w + li + 1)] + 5 * sh[((lj - 2) * w + li + 2)]

                  + 18 * sh[((lj - 1) * w + li - 2)] + 64 * sh[((lj - 1) * w + li - 1)] + 100 * sh[((lj - 1) * w + li)] + 64 * sh[((lj - 1) * w + li + 1)] + 18 * sh[((lj - 1) * w + li + 2)]

                  + 5 * sh[(lj * w + li - 3)] + 32 * sh[(lj * w + li - 2)] + 100 * sh[(lj * w + li - 1)] + 100 * sh[(lj * w + li)] + 100 * sh[(lj * w + li + 1)] + 32 * sh[(lj * w + li + 2)] + 5 * sh[(lj * w + li + 3)]

                  + 18 * sh[((lj + 1) * w + li - 2)] + 64 * sh[((lj + 1) * w + li - 1)] + 100 * sh[((lj + 1) * w + li)] + 64 * sh[((lj + 1) * w + li + 1)] + 18 * sh[((lj + 1) * w + li + 2)]

                  + 5 * sh[((lj + 2) * w + li - 2)] + 18 * sh[((lj + 2) * w + li - 1)] + 32 * sh[((lj + 2) * w + li)] + 18 * sh[((lj + 2) * w + li + 1)] + 5 * sh[((lj + 2) * w + li + 2)]

                  + 5 * sh[((lj + 3) * w + li)]) /
              1068);
    auto res = h * h;
    res = res > 65025 ? res = 65025 : res;

    result[j * cols + i] = sqrtf(res);
  }
}

int main()
{

  // cv::Mat m_in = cv::imread("Images/Input/test-14400x7200.jpg", cv::IMREAD_UNCHANGED );
  // cv::Mat m_in = cv::imread("Images/Input/in.jpg", cv::IMREAD_UNCHANGED );
  cv::Mat m_in = cv::imread("Images/Input/taille_moyenne.jpeg", cv::IMREAD_UNCHANGED);
  cudaError_t cudaStatus;
  cudaError_t kernelStatus;

  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;

  std::vector<unsigned char> g(rows * cols);
  cv::Mat m_out(rows, cols, CV_8UC1, g.data());
  cv::Mat m_grayscale(rows, cols, CV_8UC1, g.data());

  unsigned char *rgb_d;
  unsigned char *g_d;

  cudaStatus = cudaMalloc(&rgb_d, 3 * rows * cols);
  checkError(cudaStatus, (std::string) "Error CudaMalloc rgb_d");

  cudaStatus = cudaMalloc(&g_d, rows * cols);
  checkError(cudaStatus, (std::string) "Error CudaMalloc g_d");

  cudaStatus = cudaMemcpy(rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice);
  checkError(cudaStatus, (std::string) "Error cudaMemcpy H2D rgb_d");

  dim3 block(32, 32);
  // grid 0 pour mémoire non partagée
  dim3 grid0((cols - 1) / block.x + 1, (rows - 1) / block.y + 1);
  // grid 1 pour mémoire partagée avec x-6 et y-6 afin de pouvoir appliquer la matrice de taille 7*7 du filtre
  dim3 grid1((cols - 1) / (block.x - 6) + 1, (rows - 1) / (block.y - 6) + 1);

  std::size_t const size = cols * rows;
  std::size_t const sizeb = size * sizeof(char);

  // v0 et v0_d sont les tableaux contenant grayscale, v0 se trouve sur la mémoire CPU et v0_d sur la mémoire GPU
  // v1 et v1_d sont les tableaux dans lesquels on écrit le résultat du filtre, v1 se trouve sur la mémoire CPU et v1_d sur la mémoire GPU
  unsigned char *v0 = nullptr;
  unsigned char *v1 = nullptr;

  unsigned char *v0_d = nullptr;
  unsigned char *v1_d = nullptr;

  cudaStatus = cudaMalloc(&v0_d, sizeb);
  checkError(cudaStatus, (std::string) "Error CudaMalloc v0_d");

  cudaStatus = cudaMalloc(&v1_d, sizeb);
  checkError(cudaStatus, (std::string) "Error CudaMalloc v1_d");

  cudaStatus = cudaMallocHost(&v0, sizeb);
  checkError(cudaStatus, (std::string) "Error CudaMalloc v0");

  cudaStatus = cudaMallocHost(&v1, sizeb);
  checkError(cudaStatus, (std::string) "Error CudaMalloc v1");
  //création des streams
  cudaStream_t streams[2];

  cudaStreamCreate(&streams[0]);
  cudaStreamCreate(&streams[1]);

  cudaEvent_t start, stop;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  grayscale<<<grid0, block>>>(rgb_d, g_d, cols, rows);
  cudaStatus = cudaMemcpy(g.data(), g_d, rows * cols, cudaMemcpyDeviceToHost);
  checkError(cudaStatus, (std::string) "Error cudaMemcpy D2H g_d");

  for (std::size_t i = 0; i < size; ++i)
  {
    v0[i] = g.data()[i];
  }

  cv::imwrite("Images/Output/out_grayscale.jpg", m_grayscale);

  // copie de chaque moitiée du tableau dans un stream différent
  // stream[0] calculera la première moitiée et streams[1] la deuxième et un décalage de 3 lignes est appliqué afin de pouvoir appliquer la matrice sur les points en extrémités

  cudaStatus = cudaMemcpyAsync(v0_d, v0, ((size / 2) + 3 * cols) * sizeof(char), cudaMemcpyHostToDevice, streams[0]);
  checkError(cudaStatus, (std::string) "Error cudaMemcpyAsync H2D v0_d 1/2");
  cudaStatus = cudaMemcpyAsync((v0_d + (size / 2)) - 3 * cols, (v0 + (size / 2)) - 3 * cols, ((size / 2) + 3 * cols) * sizeof(char), cudaMemcpyHostToDevice, streams[1]);
  checkError(cudaStatus, (std::string) "Error cudaMemcpyAsync H2D v0_d 2/2");

  cudaEventRecord(start, 0);
  /*
  version streams seulement

  gaussianBlur<<< grid0, block, 0, streams[ 0 ] >>>( v0_d, v1_d, cols, ((rows/2)+3));
  gaussianBlur<<< grid0, block, 0, streams[ 1 ] >>>( (v0_d+size/2)-3*cols, v1_d+(size/2)-3*cols, cols, ((rows/2)+3));
  */

  // version stream avec memoire partagée
  gaussianBlur_shared<<<grid1, block, block.x * block.y, streams[0]>>>(v0_d, v1_d, cols, ((rows / 2) + 3));
  gaussianBlur_shared<<<grid1, block, block.x * block.y, streams[1]>>>((v0_d + size / 2) - 3 * cols, v1_d + (size / 2) - 3 * cols, cols, ((rows / 2) + 3));

  //synchronization des streams afin d'avoir le temps de calcul
  cudaStreamSynchronize(streams[0]);
  cudaStreamSynchronize(streams[1]);

  cudaEventRecord(stop, 0);

  // copie de mémoire des streams vers le CPU, on remplit le tableau avec les deux moitiées calculées
  cudaStatus = cudaMemcpyAsync(v1, v1_d, (size / 2) * sizeof(char), cudaMemcpyDeviceToHost, streams[0]);
  checkError(cudaStatus, (std::string) "Error cudaMemcpyAsync D2H v1_d 2/2");

  cudaStatus = cudaMemcpyAsync(v1 + (size / 2), v1_d + (size / 2), (size / 2) * sizeof(char), cudaMemcpyDeviceToHost, streams[1]);
  checkError(cudaStatus, (std::string) "Error cudaMemcpyAsync D2H v1_d 2/2");

  cudaDeviceSynchronize();

  //copie de v1 dans g.data() afin d'avoir le rendu
  cudaStatus = cudaMemcpy(g.data(), v1, rows * cols, cudaMemcpyHostToHost);
  checkError(cudaStatus, (std::string) "Error cudaMemcpyAsync D2H d.data(),v1 ");

  cudaEventSynchronize(stop);

  float duration;

  cudaEventElapsedTime(&duration, start, stop);
  std::cout << "time=" << duration << std::endl;

  cudaEventDestroy(start);
  cudaEventDestroy(stop);

  cv::imwrite("Images/Output/out_gaussian_blur_streams_cu.jpg", m_out);

  kernelStatus = cudaGetLastError();
  checkError(cudaStatus, (std::string) "CUDA Error : " + cudaGetErrorString(kernelStatus));

  cudaFree(rgb_d);
  cudaFree(g_d);
  cudaStreamDestroy(streams[0]);
  cudaStreamDestroy(streams[1]);
  cudaFree(v0_d);
  cudaFree(v1_d);

  cudaFreeHost(v0);
  cudaFreeHost(v1);

  return 0;
}
